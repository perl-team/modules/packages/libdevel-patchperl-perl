Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Devel-PatchPerl
Upstream-Contact: Chris Williams <chris@bingosnet.co.uk>
Source: https://metacpan.org/release/Devel-PatchPerl

Files: *
Copyright: 2021, Chris Williams <chris@bingosnet.co.uk>
 2021, Marcus Holland-Moritz <mhx-cpan@gmx.net>
License: Artistic or GPL-1+

Files: t/modern/patchlevel.h
Copyright: 1993-2009 Larry Wall and others
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2011, Alessandro Ghedini <ghedo@debian.org>
 2011, Jotam Jr. Trejo <jotamjr@debian.org.sv>
 2011-2021, gregor herrmann <gregoa@debian.org>
 2016, Lucas Kanashiro <kanashiro@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
